#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 22:52:41 2021


"""

import numpy as np
from numpy import inf
import sys
sys.path.append('../src')
from tf_idf import tf_idf
import unittest

class SaneEqualityArray(np.ndarray):
    def __eq__(self, other):
        return (isinstance(other, SaneEqualityArray) and self.shape == other.shape and np.ndarray.__eq__(self, other).all())

class TestTfIdf(unittest.TestCase):
    def test_TfIdf_1(self):
        docs_content=[['i', 'love', 'shanghai'], ['i', 'am', 'from', 'shanghai', 'now', 'i', 'study', 'in', 
                    'tongji', 'university'], ['i', 'am', 'from', 'lanzhou', 'now', 'i', 'study', 'in', 'lanzhou',
                    'university', 'of', 'science', 'and', 'technolgy']]
        vocabulary= {'i': 0, 'love': 1, 'shanghai': 2, 'am': 3, 'from': 4, 'now': 5, 'study': 6, 'in': 7, 'tongji': 8, 
     'university': 9, 'lanzhou': 10, 'of': 11, 'science': 12, 'and': 13, 'technolgy': 14}
        docuCount=3
        tf_matrix_classique=tf_idf.tf_classique (docs_content,vocabulary,docuCount)
        np.testing.assert_array_equal(tf_matrix_classique, np.array([[1. , 1. , 1. ],
                                                                    [1. , 0. , 0. ],
                                                                    [1. , 0.5, 0. ],
                                                                    [0. , 0.5, 0.5],
                                                                    [0. , 0.5, 0.5],
                                                                    [0. , 0.5, 0.5],
                                                                    [0. , 0.5, 0.5],
                                                                    [0. , 0.5, 0.5],
                                                                    [0. , 0.5, 0. ],
                                                                    [0. , 0.5, 0.5],
                                                                    [0. , 0. , 1. ],
                                                                    [0. , 0. , 0.5],
                                                                    [0. , 0. , 0.5],
                                                                    [0. , 0. , 0.5],
                                                                    [0. , 0. , 0.5]]))
        
    def test_TfIdf_2(self):       
        tf_matrix_classique=np.array([[1. , 1. , 1. ],
                                      [1. , 0. , 0. ],
                                      [1. , 0.5, 0. ],
                                      [0. , 0.5, 0.5],
                                      [0. , 0.5, 0.5],
                                      [0. , 0.5, 0.5],
                                      [0. , 0.5, 0.5],
                                      [0. , 0.5, 0.5],
                                      [0. , 0.5, 0. ],
                                      [0. , 0.5, 0.5],
                                      [0. , 0. , 1. ],
                                      [0. , 0. , 0.5],
                                      [0. , 0. , 0.5],
                                      [0. , 0. , 0.5],
                                      [0. , 0. , 0.5]])
        mat_nor_log=tf_idf.tf_normalisation_log (tf_matrix_classique)
        np.testing.assert_allclose( mat_nor_log,np.array([[1.        , 1.        , 1.        ],
                                                            [1.        ,       -inf,       -inf],
                                                            [1.        , 0.30685282,       -inf],
                                                            [      -inf, 0.30685282, 0.30685282],
                                                            [      -inf, 0.30685282, 0.30685282],
                                                            [      -inf, 0.30685282, 0.30685282],
                                                            [      -inf, 0.30685282, 0.30685282],
                                                            [      -inf, 0.30685282, 0.30685282],
                                                            [      -inf, 0.30685282,       -inf],
                                                            [      -inf, 0.30685282, 0.30685282],
                                                            [      -inf,       -inf, 1.        ],
                                                            [      -inf,       -inf, 0.30685282],
                                                            [      -inf,       -inf, 0.30685282],
                                                            [      -inf,       -inf, 0.30685282],
                                                            [      -inf,       -inf, 0.30685282]])  )

    def test_TfIdf_3(self):  
        tf_matrix_classique=np.array([[1. , 1. , 1. ],
                                     [1. , 0. , 0. ],
                                     [1. , 0.5, 0. ],
                                     [0. , 0.5, 0.5],
                                     [0. , 0.5, 0.5],
                                     [0. , 0.5, 0.5],
                                     [0. , 0.5, 0.5],
                                     [0. , 0.5, 0.5],
                                     [0. , 0.5, 0. ],
                                     [0. , 0.5, 0.5],
                                     [0. , 0. , 1. ],
                                     [0. , 0. , 0.5],
                                     [0. , 0. , 0.5],
                                     [0. , 0. , 0.5],
                                     [0. , 0. , 0.5]])
        mat_nor_max=tf_idf.tf_normalisation_max (tf_matrix_classique)
        np.testing.assert_array_equal( mat_nor_max,np.array([[1.  , 1.  , 1.  ],
                                                               [1.  , 0.5 , 0.5 ],
                                                               [1.  , 0.75, 0.5 ],
                                                               [0.5 , 1.  , 1.  ],
                                                               [0.5 , 1.  , 1.  ],
                                                               [0.5 , 1.  , 1.  ],
                                                               [0.5 , 1.  , 1.  ],
                                                               [0.5 , 1.  , 1.  ],
                                                               [0.5 , 1.  , 0.5 ],
                                                               [0.5 , 1.  , 1.  ],
                                                               [0.5 , 0.5 , 1.  ],
                                                               [0.5 , 0.5 , 1.  ],
                                                               [0.5 , 0.5 , 1.  ],
                                                               [0.5 , 0.5 , 1.  ],
                                                               [0.5 , 0.5 , 1.  ]]) )


    def test_TfIdf_4(self): 
        invert_index={'i': [0, 1, 2], 'love': [0], 'shanghai': [0, 1], 'am': [1, 2], 'from': [1, 2], 'now': [1, 2], 'study': [1, 2], 'in': [1, 2], 'tongji': [1], 'university': [1, 2], 'lanzhou': [2], 'of': [2], 'science': [2], 'and': [2], 'technolgy': [2]}
        vocabulary={'i': 0, 'love': 1, 'shanghai': 2, 'am': 3, 'from': 4, 'now': 5, 'study': 6, 'in': 7, 'tongji': 8, 
          'university': 9, 'lanzhou': 10, 'of': 11, 'science': 12, 'and': 13, 'technolgy': 14}
        vocaCount=15
        docuCount=3        
        mat_idf=tf_idf.idf(invert_index,vocabulary,vocaCount,docuCount)
        np.testing.assert_allclose(mat_idf,np.array([[1.46633707],
        [1.60943791],
        [1.5040774 ],
        [1.5040774 ],
        [1.5040774 ],
        [1.5040774 ],
        [1.5040774 ],
        [1.5040774 ],
        [1.60943791],
        [1.5040774 ],
        [1.60943791],
        [1.60943791],
        [1.60943791],
        [1.60943791],
        [1.60943791]])  )


if __name__ == '__main__':
    unittest.main()
    
