# -*- coding: utf-8 -*-
#	la longueur d'une lettre avec accent en python 2.7.17 vaut 2 (ex:len('à')=2) donc il faut faire le test unitaire en python3 command:  python3 -m unittest test_hamming.py

import sys
sys.path.append('../src')
from hamming import hamming
import unittest


class TestHamming(unittest.TestCase):
	def test_hamming_1(self):
			result=hamming("mais","mats")
			self.assertEqual(result,1)
	def test_hamming_2(self):
			result=hamming("événement","avénement")
			self.assertEqual(result,1)
	def test_hamming_3(self):
			result=hamming("lapin","poisson")
			self.assertEqual(result,-1)
	def test_hamming_4(self):
			result=hamming("chat","chien")
			self.assertEqual(result,-1)
			

