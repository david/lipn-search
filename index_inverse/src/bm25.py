#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 19:51:28 2021

@author: jinlili

dm-25
"""

import numpy as np

class bm25:

    def idf_2 (invert_index,vocabulary,vocaCount,docuCount):
        '''Inverse Document Frequency (IDF)
        IDF(t) = log(|D|−|Dt|+0.5/ |Dt|+0.5 )
         '''
        df = np.zeros((vocaCount, 1))
        for (key,val) in vocabulary.items():
               index_val=invert_index.get(key)
               if index_val !=None:
                    df[val,0] = len(index_val)
        idf_2=np.log((docuCount-df+0.5) / (df+0.5))
        # print((docuCount-df+0.5) / (df+0.5))
        return idf_2
    
    def bm_25 (tf_matrix_classique,docs_content,docuCount,idf_2,k=1.2 , b=0.75):
        '''   
        k1 ∈ [1.2, 2.0] (en pratique)
        b = 0.75 (en pratique)
        |d| est la longueur du document
        avgdl : Average Document Length, la longueur moyenne des documents de la collection
        BM-25t,d =IDF(t)∗( ft,d ×(k1 +1)   / ft,d + k1 × ( 1−b+b× (|d|/avgdl) ) )
        
        '''
        d_longueur=np.array([])
        for d in docs_content:
            d_longueur=np.append(d_longueur,len(d))
        sum_d=np.sum(d_longueur)
        avg_d=sum_d/np.size(d_longueur)
        bm_25=idf_2*(tf_matrix_classique*(k+1)/(tf_matrix_classique+k*(1-b+b*(d_longueur/avg_d))))
        return bm_25   


