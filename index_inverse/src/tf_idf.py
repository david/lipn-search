#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 02:16:23 2021


TF-IDE
"""

import numpy as np
from collections import Counter

class tf_idf:

    def tf_classique (docs_content,vocabulary,docuCount):
        ''' nombre d’occurrences de t dans d /nombre de mots dans d'''
        tf_matrix = np.zeros((len(vocabulary),docuCount), dtype=np.float64)    # [n_vocab, n_doc]
        for id, doc in zip(range(docuCount),docs_content):
            counter = Counter(doc)
            for v in counter.keys():
                 tf_matrix[vocabulary[v],id] = counter[v] / counter.most_common(1)[0][1]           
        return tf_matrix
    
    def tf_normalisation_log (tf_matrix_classique):
        '''Normalisation Logarithmique: 1 + log ft ,d '''
        tf_matrix_log=np.log(tf_matrix_classique)+1
        return tf_matrix_log
    
    def tf_normalisation_max (tf_matrix_classique):
        ''' Normalisationpar le max : 0.5 + 0.5 × ft,d/maxt′∈d ft′,d  '''
        tf_matrix_max=  0.5 + 0.5 * (tf_matrix_classique / np.max(tf_matrix_classique, axis=1, keepdims=True))
        return tf_matrix_max
    
    def idf (invert_index,vocabulary,vocaCount,docuCount):
        '''Inverse Document Frequency (IDF)
         Soit un terme t et une collection de documents D, DFt =log(D/{d ∈ D | t apparaˆıt dans d})
         '''
        df = np.zeros((vocaCount, 1))
        for (key,val) in vocabulary.items():
            index_val=invert_index.get(key)
            if index_val !=None:
                df[val,0] = len(index_val)
        idf=np.log(docuCount+1/df+1)  #Le dénominateur ne peut pas être zéro
        return idf





