from flask import Flask, render_template

# Initialize the Flask application
app = Flask(__name__, template_folder='.')

@app.route('/')
def index():
    return render_template('/index.html')
@app.route('/index')
def route_index():
    return render_template('/index.html')

if __name__ == '__main__':
    app.run(
        host="localhost",
        port=int("8011")
        #,debug=True
    )
