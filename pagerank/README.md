Le répertoire src contient les sources du programme qui crée un graphe
des pages web du lipn et en calcule le pagerank.

Le répertoire test contient les tests unitaires qui vérifie que les fonctions
dans contenues dans le répertoire src jouent correctement leur rôle


Les bibliothèques requises sont celles de base en C, stdlib, stdio, string, time et assert. (.h)

Pour compiler les fichiers de test, il faut se placer dans le répertoire test et entrer la commande make

Le programme final, quand il sera fait se compilera en faisant make dans le répertoire pagerank
