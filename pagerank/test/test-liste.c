#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "../src/liste_url.h"


int main()
{
	liste_url l;

	char* google = "www.google.com";
	char* pasGoogle = "www.gogol.com";
	char* fichierNico = "file://C:\\Users\\Nicolas\\Homework";
	char* lipn = "lipn.univ-paris13.fr/";
	char* lipnPresentation = "https://lipn.univ-paris13.fr/accueil/presentation/le-laboratoire/";


	creer_liste(&l, 1);
	liste_add(&l, google);
	assert(!strcmp(l.urls[0], google));

	liste_add(&l, pasGoogle);
	assert(!strcmp(l.urls[1], pasGoogle));
	
	liste_add(&l, fichierNico);
	assert(!strcmp(l.urls[2], fichierNico));

	liste_add(&l, lipn);
	assert(!strcmp(l.urls[3], lipn));

	liste_add(&l, lipnPresentation);
	assert(!strcmp(l.urls[4], lipnPresentation));


	liste_afficher(&l);

	printf("La première URL est %s\n", liste_get_i(&l, 0));

	printf("La liste %s l'URL \"www.google.com\", indice : %d\n", (liste_contains(&l, google) ? "contient" : "ne contient pas"), liste_find(&l, google));
	assert(liste_contains(&l, google));

	printf("Retirons la première URL, celle de google\n");

	liste_remove(&l, google);
	assert(strcmp(l.urls[0], google)); //strcmp != 0 => chaînes différentes (inférieures ou supérieures)

	liste_afficher(&l);
	printf("La liste %s l'URL \"www.google.com\", indice : %d\n", (liste_contains(&l, google) ? "contient" : "ne contient pas"), liste_find(&l, google));
	assert(!liste_contains(&l, google));

	liste_detruire(&l);
	return EXIT_SUCCESS;
}
