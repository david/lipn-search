#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../src/graphe.h"

/*
dot -Tpdf graphe_1.dot -o graphe_1.pdf
dot -Tpdf graphe_2.dot -o graphe_2.pdf
dot -Tpdf graphe_3.dot -o graphe_3.pdf
*/

#define NB_SOMMETS 7

void test_creation(){
  	//Test création
  graphe g;
  creer_graphe(&g, NB_SOMMETS);
  assert(g.max > 0);
  assert(g.premierVide == 0);
  assert(g.ligne);
  assert(g.col);
}


int main()
{
	graphe g;

	char* fichier1 = "graphe_1.dot";
	assert(fichier1);
	char* fichier2 = "graphe_2.dot";
	assert(fichier2);
	char* fichier3 = "graphe_3.dot";
	assert(fichier3);

	test_creation();

	//Test insertion
	graphe_ajouter_arete(&g, 0, 1);
	assert(graphe_est_succ(&g, 0, 1));
	assert(!graphe_est_succ(&g, 1, 0));
	assert(graphe_voisins(&g, 0, 1));
	assert(g.ligne[0] == 0);
	assert(g.col[0] == 1);
	assert(g.premierVide == 1);
	assert(graphe_get_plus_grand_sommet(&g) == 1);

	graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 0, 9);
	assert(graphe_est_succ(&g, 0, 9));
	assert(graphe_voisins(&g, 0, 9));
	assert(g.ligne[1] == 0);
	assert(g.col[1] == 9);
	assert(g.premierVide == 2);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 1, 3);
	assert(graphe_est_succ(&g, 1, 3));
	assert(graphe_voisins(&g, 1, 3));
	assert(g.ligne[2] == 1);
	assert(g.col[2] == 3);
	assert(g.premierVide == 3);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 1, 2);
	assert(graphe_est_succ(&g, 1, 2));
	assert(graphe_voisins(&g, 1, 2));
	assert(g.ligne[3] == 1);
	assert(g.col[3] == 2);
	assert(g.premierVide == 4);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 1, 4);
	assert(graphe_est_succ(&g, 1, 4));
	assert(graphe_voisins(&g, 1, 4));
	assert(g.ligne[4] == 1);
	assert(g.col[4] == 4);
	assert(g.premierVide == 5);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 2, 4);
	assert(graphe_est_succ(&g, 2, 4));
	assert(graphe_voisins(&g, 2, 4));
	assert(g.ligne[5] == 2);
	assert(g.col[5] == 4);
	assert(g.premierVide == 6);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 3, 4);
	assert(graphe_est_succ(&g, 3, 4));
	assert(graphe_voisins(&g, 3, 4));
	assert(g.ligne[6] == 3);
	assert(g.col[6] == 4);
	assert(g.premierVide == 7);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 3, 5);
	assert(graphe_est_succ(&g, 3, 5));
	assert(graphe_voisins(&g, 3, 5));
	assert(g.ligne[7] == 3);
	assert(g.col[7] == 5);
	assert(g.premierVide == 8);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 4, 5);
	assert(graphe_est_succ(&g, 4, 5));
	assert(graphe_voisins(&g, 4, 5));
	assert(g.ligne[8] == 4);
	assert(g.col[8] == 5);
	assert(g.premierVide == 9);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	//graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 4, 6);
	assert(graphe_est_succ(&g, 4, 6));
	assert(graphe_voisins(&g, 4, 6));
	assert(g.ligne[9] == 4);
	assert(g.col[9] == 6);
	assert(g.premierVide == 10);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	graphe_afficher_tableaux(&g);
	graphe_ajouter_arete(&g, 5, 6);
	assert(graphe_est_succ(&g, 5, 6));
	assert(graphe_voisins(&g, 5, 6));
	assert(g.ligne[10] == 5);
	assert(g.col[10] == 6);
	assert(g.premierVide == 11);
	assert(graphe_get_plus_grand_sommet(&g) == 9);

	graphe_afficher_tableaux(&g);

	graphe_afficher(&g);


	//Test contains
	printf("Le graphe %s le sommet 9, indice : %d\n", (graphe_contains(&g, 9) ? "contient" : "ne contient pas"), graphe_find(&g, 9));

	printf("On stocke le graphe tel qu'il est après tous les ajouts dans le fichier %s\n", fichier1);
	graphe_ecrire_dot(&g, fichier1);

	printf("Retirons le sommet 4 (un nouveau sommet 4 prendra sa place\n");

	//Test retirer sommet
	graphe_retirer_sommet(&g, 4);
	assert(graphe_get_plus_grand_sommet(&g) == 8);

	printf("On stocke le graphe tel qu'il est après la suppression du sommet 4 dans le fichier %s\n", fichier2);
	graphe_ecrire_dot(&g, fichier2);

	//Test retirer arêtes
	printf("Retirons les arêtes 4-5, 1-3, et 5-4 (déjà retirée)\n");
	graphe_supprimer_arete(&g, 4, 5);
	graphe_afficher(&g);
	graphe_supprimer_arete(&g, 1, 3);
	graphe_afficher(&g);
	graphe_supprimer_arete(&g, 5, 4);
	graphe_afficher(&g);

	//Test voisins et est_succ
	printf("Les sommets 1 et 7 %s\n", (graphe_voisins(&g, 1, 7) ? "sont voisins" : "ne sont pas voisins"));
	assert(!graphe_voisins(&g, 1, 7));
	printf("Ajoutons une arête 1-7\n");
	graphe_ajouter_arete(&g, 1, 7);
	assert(graphe_est_succ(&g, 1, 7));
	printf("Les sommets 1 et 7 %s\n", (graphe_voisins(&g, 1, 7) ? "sont voisins" : "ne sont pas voisins"));
	assert(graphe_voisins(&g, 1, 7));
	graphe_afficher(&g);
	printf("Le graphe %s le sommet 9, indice : %d\n", (graphe_contains(&g, 9) ? "contient" : "ne contient pas"), graphe_find(&g, 9));

	printf("On stocke le graphe tel qu'il est après des retraits et l'ajout du sommet 7 dans le fichier %s\n", fichier3);
	graphe_ecrire_dot(&g, fichier3);

	graphe_detruire(&g);
	return EXIT_SUCCESS;
}
