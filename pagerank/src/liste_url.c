#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "liste_url.h"


/** Fonction qui crée une liste d'url qui est stockée à l'adresse donnée en argument, déjà allouée, la liste aura déjà n cases pour stocker des URLs (c'est un tableau dynamique, sa taille pourra être amenée à augmenter)
	* @param l une liste d'url allouée en mémoire
	* @param n un entier strictement positif, la taille de la liste
	* @return 0 si la liste a été créée et -1 si n <= 0 et -2 s'il y a eu une erreur d'allocation
	* O(n)
	*/
int creer_liste(liste_url* l, int n) {
	if(n <= 0) {
		printf("Erreur paramètre creer_liste, n <= 0\n");
		return -1;
	}
	if(!(l->urls = (char**) calloc(sizeof(char*), n))) {
		printf("Erreur allocation creer_liste, l->urls\n");
		return -2;
	}
	l->max = n;
	l->premierNULL = 0;
	return 0;
}


/** Renvoie la taille maximale actuelle de la liste l
	* @param l un pointeur vers une liste allouée en mémoire
	* @return g->max un entier
	* O(1)
	*/
int liste_get_max(liste_url* l) {
	return l->max;
}


/** Renvoie l'indice du premier NULL dans le liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @return l->premierNULL un entier
	* O(1)
	*/
int liste_get_premier_NULL(liste_url* l) {
	return l->premierNULL;
}


/** Indique si une url est dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL dont on veut savoir si elle est dans la liste
	* @return 1 si l'URL est dans la liste et 0 sinon
	* O(n)
	*/
int liste_contains(liste_url* l, char* s) {
	return (liste_find(l, s) != -1);
}


/** Donne l'indice d'une url est dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL dont on veut connaître l'indice dans la liste
	* @return l'indice de l'URL si elle est dans la liste et -1 sinon
	* O(n)
	*/
int liste_find(liste_url* l, char* s) {
	if(l && s) {
		int i;
		for(i = 0; i < liste_get_premier_NULL(l); i++) {
			if(!strcmp(s, liste_get_i(l, i))) {
				return i;
			}
		}
	}
	return -1;
}


/** Renvoie l'url à l'indice i dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param i un entier
	* @return l->urls[i] une chaîne de caractères si 0 <= i < liste_get_max(l), NULL sinon
	* O(1)
	*/
char* liste_get_i(liste_url* l, int i) {
	if(0 <= i && i < liste_get_max(l)) {
		return l->urls[i];
	}
	return NULL;
}


/** Ajoute une url à la première case vide dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL à ajouter
	* O(1)
	*/
void liste_add(liste_url* l, char* s) {
	if(l) {
		if(liste_get_premier_NULL(l) == liste_get_max(l)) {	//Réallocation si nécessaire, complexité amortie en temps linéaire
			l->max *= 2;
			char** nouvelleListe = (char**) calloc(sizeof(char*), liste_get_max(l));

			for (int i = 0; i < liste_get_premier_NULL(l); i++) {
				nouvelleListe[i] = (char*) malloc(sizeof(char) * (strlen(l->urls[i]) + 1));
				strcpy(nouvelleListe[i], l->urls[i]);
				free(l->urls[i]);
			}
			nouvelleListe[l->premierNULL] = (char*) calloc(sizeof(char), strlen(s)+1);
			strcpy(nouvelleListe[l->premierNULL++], s);
			free(l->urls);
			l->urls = nouvelleListe;
			return;
		}
		l->urls[l->premierNULL] = (char*) calloc(sizeof(char), strlen(s)+1);
		strcpy(l->urls[l->premierNULL++], s);
	}
}


/** Retire une url de la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL à retirer
	* O(n)
	*/
void liste_remove(liste_url* l, char* s) {
	if(l && s) {
		int i, trouve = 0;
		for(i = 0; i < liste_get_premier_NULL(l); i++) {
			if(!trouve && (!strcmp(s, liste_get_i(l, i)))) {
				free(liste_get_i(l, i));
				if(i+1 < liste_get_max(l)) {
					l->urls[i] = l->urls[i+1];
				}
				else {
					l->urls[i] = NULL;
				}
				trouve = 1;
			}
			else if(trouve) {
				if(i+1 < liste_get_max(l)) {
					l->urls[i] = l->urls[i+1];
				}
				else {
					l->urls[i] = NULL;
				}
			}
		}
		l->premierNULL--;
	}
}


/** Libère la mémoire allouée dans une liste et toutes les chaînes contenues dedans
	* @param l une liste d'url allouée en mémoire
	* O(n)
	*/
void liste_detruire(liste_url* l) {
	if(l) {
		int i;
		for(i = 0; i < liste_get_premier_NULL(l); i++) {
			free(l->urls[i]);
			l->urls[i] = NULL;
		}
		free(l->urls);
	}
}


/** Affiche la liste d'URLs
	* @param l un pointeur vers une liste allouée en mémoire
	* O(n)
	*/
void liste_afficher(liste_url* l) {
	if(l) {
		int i, n = liste_get_max(l);
		printf("i\t|\tURLs:\n");
		for(i = 0; i < n; i++) {
			if(i < liste_get_premier_NULL(l)) {
				printf("%d \t|\t\"%s\"\n", i, liste_get_i(l, i));
			}
			else {
				printf("%d \t|\tNULL\n", i);
			}
		}
	}
	else {
		printf("La liste est NULL\n");
	}
}
