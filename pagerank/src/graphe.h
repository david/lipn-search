#ifndef GRAPHE_H
#define GRAPHE_H


struct s_graphe {
	int max;					// Longueur de chacun des tableaux
	int premierVide;	// Indice du premier (-1 -1)
	int *ligne;				// Indices i {-1 si non défini, entre 0 et n-1 sinon}
	int *col;					// Indices j {-1 si non défini, entre 0 et n-1 sinon}
};
typedef struct s_graphe graphe;

/** Fonction qui crée un graphe vide qui sera stocké à l'adresse g déjà allouée
	*	@param g un pointeur vers un graphe alloué en mémoire
	*	@param n le nombre strictement positif de cases des tableaux du graphe
	*	@requires n > 0
	* @return 0 si le graphe a été créé et -1 si n <= 0 et -2 s'il y a eu une erreur d'allocation
	* O(n)
	*/
int creer_graphe(graphe* g, int n);


/** Libère la mémoire allouée dans un graphe, mais pas le graphe lui même (le pointeur donné en paramètre reste à libérer
	* @param g un pointeur vers un graphe alloué en mémoire
	* O(n);
	*/
void graphe_detruire(graphe* g);


/** Renvoie la taille maximale actuelle des tableaux du graphe g
	* @param g un pointeur vers un graphe alloué en mémoire
	* @return g->max un entier
	* O(1)
	*/
int graphe_get_max(graphe* g);


/** Renvoie l'indice du premier (-1 -1) dans le graphe
	* @param g un pointeur vers un graphe alloué en mémoire
	* @return g->premierVide un entier
	* O(1)
	*/
int graphe_get_premier_vide(graphe* g);


/** Indique si un sommet est dans le graphe
	* @param g un pointeur vers un graphe alloué en mémoire
	* @param u un entier, le sommet dont on veut savoir s'il est dans le graphe
	* @return 1 si le sommet est dans le graphe et 0 sinon
	* O(n)
	*/
int graphe_contains(graphe* g, int u);


/** Donne l'indice de la première occurence d'un sommet dans le graphe
	* @param g un pointeur vers un graphe alloué en mémoire
	* @param u un entier, le sommet dont on veut connaître l'indice dans le graphe
	* @return l'indice de le sommet si il est dans le graphe et -1 sinon
	* O(n)
	*/
int graphe_find(graphe* g, int u);


/** Renvoie le plus grand indice des sommets du graphe
	* @param g un pointeur vers un graphe alloué en mémoire
	* @return un entier
	* O(n)
	*/
int graphe_get_plus_grand_sommet(graphe* g);


/** Ajoute une arête entre les sommets i et j du graphe
	* @param g un pointeur vers un graphe alloué en mémoire
	* @param i un des sommets de l'arête
	* @param j un des sommets de l'arête
	* O(1)
	*/
void graphe_ajouter_arete(graphe* g, int i, int j);


/** Supprime une arête entre les sommets i et j du graphe
	* @param g un pointeur vers un graphe alloué en mémoire
	* @param i un des sommets de l'arête
	* @param j un des sommets de l'arête
	* O(n)
	*/
void graphe_supprimer_arete(graphe* g, int i, int j);


/** Retasse la matrice creuse contenant des -1
	*	@param g un pointeur vers un graphe alloué en mémoire
	* @param casesVides un tableau contenant les indices des cases vides de la matrice
	* @param nbCasesVides le nombre de cases vides à retasser
	* O(n)
	*/
void graphe_retasser(graphe* g, int* casesVides, int nbCasesVides);


/** Retire les instances du sommet x dans le graphe et les remplace par -1 -1
	* Indique dans un tableau quelles cases sont devenues vides, et le nombre de nouvelles cases vides
	* Puis les sommets supérieurs sont renommés, ex : si x = 5, 6->5, 7->6 etc
	*	@param g un pointeur vers un graphe alloué en mémoire
	* @param casesVides un tableau dans lequel on mettra les indices des cases vides de la matrice
	* @param nbCasesVides un pointeur vers le nombre de cases vides
	*	@param x le sommet à retirer
	* O(n)
	*/
void graphe_traitement_retirer_x(graphe* g, int* casesVides, int* ptrNbCasesVides, int x);


/** Retire un sommet du graphe, assure qu'aucun sommet n'a d'arête allant vers ce dernier et qu'aucune arête ne part de celui ci.
	* Puis les sommets supérieurs sont renommés, ex : si x = 5, 6->5, 7->6 etc
	*	@param g un pointeur vers un graphe alloué en mémoire
	*	@param x le sommet à retirer
	* O(n)
	*/
void graphe_retirer_sommet(graphe* g, int x);


/** Indique si 2 sommets sont adjacents ou non, s'il existe une arête les reliant
	* @param g un pointeur vers un graphe alloué en mémoire
	* @param u un des sommets de l'arête
	* @param v un des sommets de l'arête
	* @return 1 si les sommets sont adjacents et 0 sinon
	* O(n)
	*/
int graphe_voisins(graphe* g, int u, int v);


/** Indique si 1 sommet est successeur d'un autre ou non
	* @param g un pointeur vers un graphe alloué en mémoire
	* @param u un des sommets de l'arête
	* @param v un des sommets de l'arête
	* @return 1 si v est un successeur de u et 0 sinon
	* O(n)
	*/
int graphe_est_succ(graphe* g, int u, int v);


/** Crée la matrice d'adjacence d'un graphe
	*	@param g un pointeur vers un graphe alloué en mémoire
	* @return une matrice d'entiers, la matrice d'adjacence du graphe
	* O(n)
	*/
int* graphe_to_Adj_Matrix(graphe* g);


/** Affiche une matrice d'adjacence
	*	@param matrice un pointeur vers la matrice
	*	@param lim, un entier qui est la taille des côtés de la matrice
	* O(n)
	*/
void graphe_afficher_Matrice_Adj(int* matrice, int lim);


/** Affiche le graphe sous la forme d'une matrice d'adjacence
	*	@param g un pointeur vers un graphe alloué en mémoire
	* O(n)
	*/
void graphe_afficher(graphe* g);


/** Affiche un tableau
	*	@param t un pointeur vers un tableau d'entiers
	* @param premierVide un entier le nombre d'éléments non nuls
	* @param taille un entier la taille du tableau
	* O(n)
	*/
void graphe_afficher_Un_Des_Tableaux(int* t, int premierVide, int taille);


/** Affiche le graphe sous la forme des 2 tableaux le composant
	*	@param g un pointeur vers un graphe alloué en mémoire
	* O(n)
	*/
void graphe_afficher_tableaux(graphe* g);


/** Donne la liste des sommets du graphe, sans répétitions dans le tableau listeSommets passé en argument pour les indiquer dans le fichier DOT
	* @param g un graphe alloué en mémoire
	* @param flagSommet un tableau qui indique pour chacun de ces indices, si le sommet correspondant est dans le graphe
	* @param listeSommets la liste des sommets du graphe, au max il y en a 2n, où n = l'indice du premierVide du graphe
	* @param ptrNbSommets un pointeur vers un entier qui est le nombre de sommets distincts du graphe
	* O(n)
	*/
void graphe_liste_sommets_dot(graphe* g, int* flagSommet, int* listeSommets, int* ptrNbSommets);


/** Convertit un graphe au format DOT dans un fichier
	* @param g un graphe alloué en mémoire
	* @param nomFichier, le nom du fichier où l'on veut écrire le graphe
	* @return 0 si le graphe a bien été écris au format DOT,
	*	-1 s'il y a eu une erreur d'ouverture du fichier, et -2 sinon
	* O(n)
	*/
int graphe_ecrire_dot(graphe* g, char* nomFichier);


/** Renvoie le maximum entre 2 entiers
	* @param x un entier
	* @param y un entier
	* @return un entier, le plus grand des 2 paramètres
	* O(1)
	*/
int max(int x, int y);

#endif
