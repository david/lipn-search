#ifndef LISTE_URL_H
#define LISTE_URL_H


struct s_liste_url {
	int max;					// Longueur de la liste allouée
	int premierNULL;	// Indice de la première chaîne NULL
	char** urls;			// Les urls de la liste
};
typedef struct s_liste_url liste_url;


/** Fonction qui crée une liste d'url qui est stockée à l'adresse donnée en argument, déjà allouée, la liste aura déjà n cases pour stocker des URLs (c'est un tableau dynamique, sa taille pourra être amenée à augmenter)
	* @param l une liste d'url allouée en mémoire
	* @param n un entier strictement positif, la taille de la liste
	* @return 0 si la liste a été créée et -1 si n <= 0 et -2 s'il y a eu une erreur d'allocation
	* O(n)
	*/
int creer_liste(liste_url* l, int n);


/** Renvoie la taille maximale actuelle de la liste l
	* @param l un pointeur vers une liste allouée en mémoire
	* @return g->max un entier
	* O(1)
	*/
int liste_get_max(liste_url* l);


/** Renvoie l'indice du premier NULL dans le liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @return l->premierNULL un entier
	* O(1)
	*/
int liste_get_premier_NULL(liste_url* l);


/** Indique si une url est dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL dont on veut savoir si elle est dans la liste
	* @return 1 si l'URL est dans la liste et 0 sinon
	* O(n)
	*/
int liste_contains(liste_url* l, char* s);


/** Donne l'indice d'une url est dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL dont on veut connaître l'indice dans la liste
	* @return l'indice de l'URL si elle est dans la liste et -1 sinon
	* O(n)
	*/
int liste_find(liste_url* l, char* s);


/** Renvoie l'url à l'indice i dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param i un entier
	* @return l->urls[i] une chaîne de caractères si 0 <= i < liste_get_max(l), NULL sinon
	* O(1)
	*/
char* liste_get_i(liste_url* l, int i);


/** Ajoute une url à la première case vide dans la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL à ajouter
	* O(1)
	*/
void liste_add(liste_url* l, char* s);


/** Retire une url de la liste
	* @param l un pointeur vers une liste allouée en mémoire
	* @param s une chaîne, l'URL à retirer
	* O(n)
	*/
void liste_remove(liste_url* l, char* s);



/** Libère la mémoire allouée dans une liste et toutes les chaînes contenues dedans
	* @param l une liste d'url allouée en mémoire
	* O(n)
	*/
void liste_detruire(liste_url* l);


/** Affiche la liste d'URLs
	* @param l un pointeur vers une liste allouée en mémoire
	* O(n)
	*/
void liste_afficher(liste_url* l);

#endif
